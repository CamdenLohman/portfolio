
#include "Enemy.h"
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>

#pragma region Enemy
Enemy::Enemy(float x, float y, int type, int state)
{
	//Initialize the position
	mPosX = x;
	mPosY = y;

	e_type = type;
	e_state = state;

	if (e_type == 1)
	{
		mVelX = 0;
		ENEMY_HEIGHT = 16;
		ENEMY_WIDTH = 16;
	}
	if (e_type == 2)
	{
		mVelX = 0;
		ENEMY_HEIGHT = 24;
		ENEMY_WIDTH = 16;
	}

	//Set collision box dimension
	mCollider.w = ENEMY_WIDTH;
	mCollider.h = ENEMY_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;




}


void Enemy::move(float timeStep, vector<Wall>Walls, vector<Wall>transfer_Up, vector<Wall>transfer_Down, vector<Wall>transfer_Left, vector<Wall>transfer_Right)
{
	//temp
	const int TILE_WIDTH = 16;
	const int TILE_HEIGHT = 16;
	int LEVEL_WIDTH = TILE_WIDTH * 212;
	int LEVEL_HEIGHT = TILE_HEIGHT * 15;
	int MAX_VEL = 800;
	if (mPosX <= 0)
	{
		moving_left = false;
	}

	if (mPosX >= LEVEL_WIDTH - ENEMY_WIDTH)
	{
		moving_left = true;
	}
	//Move the dot left or right
	mPosX += mVelX * timeStep;
	mCollider.x = mPosX;

	//Move the dot up or down

	if (mVelY <= MAX_VEL)
	{

		mVelY += 80;
	}
	else
	{
		mVelY = MAX_VEL;
	}
	mPosY += mVelY * timeStep;
	mCollider.y = mPosY;

}



void Enemy::render(SDL_Rect camera, SDL_Renderer* gRenderer, SDL_RendererFlip flip, LTexture gEnemyTexture, SDL_Rect* EnemycurrentClip)
{
	gEnemyTexture.render(mPosX - camera.x, mPosY - camera.y, EnemycurrentClip, NULL, NULL, flip);

	//gEnemyTexture.render(mPosX - camX, mPosY - camY, gGoombaWalk, NULL, NULL, EnemyFlipType);

}

int Enemy::getPosX()
{
	return mPosX;
}

int Enemy::getPosY()
{
	return mPosY;
}

void Enemy::SetTexture(string text){
	Texture.loadFromFile(text);
}

LTexture Enemy::GetTexture(){
	return Texture;
}
#pragma endregion
