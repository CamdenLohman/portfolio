#include <math.h>
#include <assert.h>
#include <glm\glm.hpp>

struct Vec3
{
	Vec3() {}
	Vec3(float x, float y, float z) : x(x), y(y), z(z) {}

	void Set(float x_, float y_, float z_) { x = x_; y = y_; z = z_; }

	Vec3 operator -() const { return Vec3(-x, -y, -z); }

	void operator += (const Vec3& v)
	{
		x += v.x; y += v.y; z += v.z;
	}

	void operator -= (const Vec3& v)
	{
		x -= v.x; y -= v.y; z -= v.z;
	}

	void operator *= (float a)
	{
		x *= a; y *= a; z *= a;
	}

	float Length() const
	{
		return sqrtf(x * x + y * y + z * z);
	}

	float x, y, z;
};

	struct Mat33
	{
		Mat33() {}
		Mat33(const Vec3& col1, const Vec3& col2, const Vec3& col3) : col1(col1), col2(col2), col3(col3) {}

		void Set(float angle)
		{
			float c = cosf(angle), s = sinf(angle);
			col1.x = c; col2.x = -s; col3.x = 0;
			col1.y = s; col2.y = c; col3.y = 0;
			col1.z = 0; col2.z = 0; col3.z = 0;
		}

		Vec3 col1, col2, col3;
	};

	struct Transform
	{
		Mat33 R;
		Vec3 p;
	};

	/// A simplex vertex.
	struct SimplexVertex
	{
		Vec3 point1;	// support point in polygon1
		Vec3 point2;	// support point in polygon2
		Vec3 point;		// point2 - point1
		float u;		// unnormalized barycentric coordinate for closest point
		int index1;		// point1 index
		int index2;		// point2 index
	};

	/// Simplex used by the GJK algorithm.
	struct Simplex
	{
		Vec3 GetSearchDirection() const;
		Vec3 GetClosestPoint() const;
		void GetWitnessPoints(Vec3* point1, Vec3* point2) const;
		void Solve2(const Vec3& P);
		void Solve3(const Vec3& P);

		SimplexVertex m_vertexA, m_vertexB, m_vertexC;
		float m_divisor;	// denominator to normalize barycentric coordinates
		int m_count;
	};

	struct Output
	{
		enum
		{
			e_maxSimplices = 20
		};

		Vec3 point1;		///< closest point on polygon 1
		Vec3 point2;		///< closest point on polygon 2
		float distance;
		int iterations;		///< number of GJK iterations used

		Simplex simplices[e_maxSimplices];
		int simplexCount;
	};

	/// Polygon used by the GJK algorithm.
	struct Polygon
	{
		/// Get the supporting point index in the given direction.
		int GetSupport(const Vec3& d) const;

		const Vec3* m_points;
		int m_count;
	};

	struct Input
	{
		Polygon polygon1;
		Polygon polygon2;
		Transform transform1;
		Transform transform2;
	};

	inline Vec3 operator + (const Vec3& a, const Vec3& b)
	{
		return Vec3(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	inline Vec3 operator - (const Vec3& a, const Vec3& b)
	{
		return Vec3(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	inline Vec3 operator * (float s, const Vec3& v)
	{
		return Vec3(s * v.x, s * v.y, s * v.z);
	}

	inline Vec3 Mul(const Mat33& A, const Vec3& v)
	{
		return Vec3(A.col1.x * v.x + A.col2.x * v.y, A.col1.y * v.x + A.col2.y * v.y, 0);
	}

	inline Vec3 Mul(const Transform& T, const Vec3& v)
	{
		return Mul(T.R, v) + T.p;
	}

	inline float Dot(const Vec3& a, const Vec3& b)
	{
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	inline float Crossf(const Vec3& a, const Vec3& b)
	{
		return a.x * b.y - a.y * b.x;
	}

	inline Vec3 Crossv(const Vec3& a, const Vec3& b)
	{
		return Vec3(a.y * b.z - a.z * b.y, a.x * b.z - a.z * b.x, a.x * b.y - a.y * b.z);
	}

	inline Vec3 Cross(const Vec3& a, float s)
	{
		return Vec3(s * a.y, -s * a.x, 0);
	}

	inline Vec3 Cross(float s, const Vec3& a)
	{
		return Vec3(-s * a.y, s * a.x, 0);
	}

	inline Vec3 MulT(const Mat33& A, const Vec3& v)
	{
		return Vec3(Dot(A.col1, v), Dot(A.col2, v), Dot(A.col3, v));
	}

	inline float DistanceSquared(const Vec3& a, const Vec3& b)
	{
		Vec3 d = b - a;
		return Dot(d, d);
	}

	inline float Distance(const Vec3& a, const Vec3& b)
	{
		return sqrtf(DistanceSquared(a, b));
	}

	

		// Compute the distance between two polygons using the GJK algorithm.

	void Distance2D(Output* output, const Input& input);