[Error]
[Error] --------------------
[Error] --------------------
[Error] Add Tests
[Error] --------------------
[Error] Passed InputA / Integer
[Error] Passed InputA / Float
[Error] Passed InputA / Variable
[Error] --------------------
[Error] Passed InputB / Integer
[Error] Passed InputB / Float
[Error] Passed InputB / Variable
[Error] --------------------
[Error] Passed Result / Variable
[Error] --------------------
[Error] Passed Functional Test
[Error] --------------------
[Error] --------------------

This is what the program outputs with the "Error" being the severity of the message being posted. Usually used for actual errors it was the easiest way to display the results, will use another program to edit out the error messages and only display what is needed.
