# Kythera_&_Moon_Collider

Here you can find evidence of a program I made for the Moon Collider company for their AI system which is called Kythera.

In the first screenshot you can see a custom made level in which I could run my tests.
You'll see in the second screenshot the whole section for testing the node "Add", with using the thirs and fourth screenshots you can get a closer look at the Inputs (third screenshot) and Outputs (fourth screenshot) whch were being tested.
An example of what would be printed out has been included with the text file "Print Example".

With the video "Test run" you will see how quickly I am able to get through the Kythera system's 150+ nodes with what I have created.
